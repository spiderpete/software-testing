package st;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

//import st.EntryMap.Entry;
import st.EntryMap;
import st.TemplateEngine;

public class Task21 {
 
	private EntryMap map;

	private TemplateEngine engine;

	@Before
	public void setUp() throws Exception {
		map = new EntryMap();
		engine = new TemplateEngine();
	}
	
 
	// TASK 2 PART 2 
	
	@Test
	public void TwoConsecutiveEmptyTemplatesDelete() {
		// it checks that two consecutive empty templates are properly deleted when not matched
		map.store("name", "Adam", false);
		map.store("surname", "Dykes", false);
		String result = engine.evaluate("${}${}", map, "delete-unmatched");
		assertEquals("", result);
	}
	
	@Test
	public void TwoConsequetiveEmptyTemplatesKeep() {
		// it checks that two consecutive empty templates are properly kept when not matched
		map.store("name", "Adam", false);
		map.store("surname", "Dykes", false);
		String result = engine.evaluate("${}${}", map, "keep-unmatched");
		assertEquals("${}${}", result);
	}

		
	@Test
	public void WrongOpenTemplate() {
		//templates open in the wrong way
		map.store("Name", "Adam", false);
		map.store("Name", "Brian", true);
		String result = engine.evaluate("$  {name} \n{name} \n$      {Name}", map, "delete-unmatched");
		assertEquals("$  {name} \n{name} \n$      {Name}", result);
	}
		
	@Test
	public void NestedKeep() {
		// checks that the code can deal with multiple nested templates when keeping the unmatched
		map.store("name", "surname", false);
		map.store("surname", "name", false);
		String result = engine.evaluate("${${${name}}} ${something} ${${${name}}}", map, "keep-unmatched");
		assertEquals("surname ${something} surname", result);
	}
	
	@Test
	public void NestedDelete() {
		// checks that the code can deal with multiple nested templates when deleting the unmatched
		map.store("name", "surname", false);
		map.store("surname", "name", false);
		String result = engine.evaluate("${${${name}}} ${something} ${${${name}}}", map, "delete-unmatched");
		assertEquals("surname  surname", result);
	}
	
	// TASK 2 PART 1

	@Test(expected = Exception.class)
	public void EmptyTestMapTemplate() {
		// Map template == empty
		// Runtime exception expected
		map.store("", "Adam", false);
	}

	@Test
	public void TemplateIfEmptyTemplateString() {
		// Tests how we deal with an empty template

		// Attempts to test the branch in the doReplace() method which covers
		// the template being at the end of the input/instanced string.
		// The test doesn't cover the branch because the indices are counted
		// from 0,
		// while the length starts from 1
		map.store("name", "Adam", false);
		map.store("surname", "Dykes", false);
		String result = engine.evaluate("${}", map, "delete-unmatched");
		assertEquals("", result);
	}

	@Test
	public void TemplateIfNull() {
		// was meant to test what happens if the sort function does not enter
		// the for loop
		// however, we were unable to come up with a case where this would
		// happen
		map.store("name", "Adam", false);
		map.store("surname", "Dykes", false);
		String result = engine.evaluate("${name}", map, "delete-unmatched");
		assertEquals("Adam", result);
	}
/*
	// Testing equals() below:

	@Test
	public void MapEquals1() {
		// Checks if duplicate entries in EntryMap are removed
		// Checks if entries are considered different when their value is
		// different
		map.store("Name", "Brian", false);
		map.store("Name", "Brian", false);
		map.store("Name", "Adam", false);
		ArrayList<Entry> entries = map.getEntries();
		assertSame(false, entries.get(0).equals(entries.get(1)));
	}

	@Test
	public void MapEquals2() {
		// Checks if the same entry is considered equal to itself
		map.store("Name", "Brian", false);
		map.store("Name", "Brian", false);
		map.store("Name", "Adam", false);
		ArrayList<Entry> entries = map.getEntries();
		assertSame(true, entries.get(0).equals(entries.get(0)));
	}

	@Test
	public void MapEquals3() {
		// checks equals when the input object is null
		map.store("Name", "Brian", false);
		ArrayList<Entry> entries = map.getEntries();
		assertSame(false, entries.get(0).equals(null));
	}

	@Test
	public void MapEquals4() {
		// checks equals works when the input object is a different class
		map.store("Name", "Brian", false);
		ArrayList<Entry> entries = map.getEntries();
		assertSame(false, entries.get(0).equals(map));
	}

	@Test
	public void MapEquals5() {
		// checks if entries are considered different when their pattern is
		// different
		map.store("Name", "Brian", false);
		map.store("not Name", "Brian", false);
		ArrayList<Entry> entries = map.getEntries();
		assertSame(false, entries.get(0).equals(entries.get(1)));
	}

	@Test
	public void MapEquals6() {
		// entries are considered equal if their case sensitivity flag is null
		EntryMap.Entry entry = map.new Entry("Name", "Brian", null);
		EntryMap.Entry entry2 = map.new Entry("Name", "Brian", null);
		assertSame(true, entry.equals(entry2));
	}

	@Test
	public void MapEquals7() {
		// entries are considered different if their case sensitivity flag is
		// different
		EntryMap.Entry entry = map.new Entry("Name", "Brian", null);
		EntryMap.Entry entry2 = map.new Entry("Name", "Brian", true);
		assertSame(false, entry.equals(entry2));
	}

	@Test
	public void EqualTemplatesNullObject() {
		// Check a template if equals against a null object
		TemplateEngine te1 = new TemplateEngine();
		TemplateEngine.Template te1Template = te1.new Template(new Integer(1), new Integer(5), "name");
		assertFalse(te1Template.equals(null));
	}

	@Test
	public void EqualTemplatesSimilarObjects() {
		// Check a template if equals against a similar template, but not same object
		TemplateEngine te1 = new TemplateEngine();
		TemplateEngine.Template te1Template = te1.new Template(new Integer(1), new Integer(5), "name");
		TemplateEngine.Template te2Template = te1.new Template(new Integer(1), new Integer(5), "name");
		assertFalse(te1Template.equals(te2Template));
	}

	@Test
	public void EqualTemplatesSameObject() {
		// Check a template if equals against the same object
		TemplateEngine te1 = new TemplateEngine();
		TemplateEngine.Template te1Template = te1.new Template(new Integer(1), new Integer(5), "name");
		TemplateEngine.Template te2Template = te1Template;
		assertTrue(te2Template.equals(te1Template));
	}

	@Test
	public void EqualTemplatesDiffClassObject() {
		// Check a template if equals against a non-template object
		TemplateEngine te1 = new TemplateEngine();
		TemplateEngine.Template te1Template = te1.new Template(new Integer(1), new Integer(5), "name");
		assertFalse(te1Template.equals(map));
	}

	@Test
	public void EqualTemplatesNullStartIndex() {
		// Check a template equals with a lacking starting index
		TemplateEngine te1 = new TemplateEngine();
		TemplateEngine.Template te1Template = te1.new Template(null, new Integer(5), "name");
		TemplateEngine.Template te2Template = te1.new Template(new Integer(1), new Integer(5), "names");
		assertFalse(te1Template.equals(te2Template));
	}

	@Test
	public void EqualTemplatesDiffStartIndex() {
		// Check a template if equal with different starting indices
		TemplateEngine te1 = new TemplateEngine();
		TemplateEngine.Template te1Template = te1.new Template(new Integer(2), new Integer(5), "name");
		TemplateEngine.Template te2Template = te1.new Template(new Integer(1), new Integer(5), "names");
		assertFalse(te1Template.equals(te2Template));
	}

	@Test
	public void EqualTemplatesNullStart_EndIndexBoth_OneContentNull() {
		// Check a template if equal with no starting or ending indices
		// Also testing for one of the templates lacking a content
		TemplateEngine te1 = new TemplateEngine();
		TemplateEngine.Template te1Template = te1.new Template(null, null, null);
		TemplateEngine.Template te2Template = te1.new Template(null, null, "names");
		assertFalse(te1Template.equals(te2Template));
	}

	@Test
	public void EqualTemplatesDiffEndIndex() {
		// Check a template if equal with different ending indices
		TemplateEngine te1 = new TemplateEngine();
		TemplateEngine.Template te1Template = te1.new Template(new Integer(1), new Integer(6), "name");
		TemplateEngine.Template te2Template = te1.new Template(new Integer(1), new Integer(5), "names");
		assertFalse(te1Template.equals(te2Template));
	}

	@Test
	public void EqualTemplatesNullEndIndex() {
		// Check a template if equals with not specified ending index
		TemplateEngine te1 = new TemplateEngine();
		TemplateEngine.Template te1Template = te1.new Template(new Integer(1), null, "names");
		TemplateEngine.Template te2Template = te1.new Template(new Integer(1), new Integer(15), "names");
		assertFalse(te1Template.equals(te2Template));
	}

	@Test
	public void EqualTemplatesNullStart_EndIndexBoth_AllContentNull() {
		// Check a template if equal with no starting or ending indices
		// Also testing for no content templates
		TemplateEngine te1 = new TemplateEngine();
		TemplateEngine.Template te1Template = te1.new Template(null, null, null);
		TemplateEngine.Template te2Template = te1.new Template(null, null, null);
		assertTrue(te1Template.equals(te2Template));
	}
*/
	// TASK 1
	@Test
	public void InitialTest1() {
		// First Default Test - general
		map.store("name", "Adam", false);
		map.store("surname", "Dykes", false);
		String result = engine.evaluate("Hello ${name} ${surname}", map, "delete-unmatched");
		assertEquals("Hello Adam Dykes", result);
	}

	@Test
	public void InitialTest2() {
		// Second Default Test - general
		map.store("name", "Adam", false);
		map.store("surname", "Dykes", false);
		map.store("age", "29", false);
		String result = engine.evaluate("Hello ${name}, is your age ${age ${symbol}}", map, "delete-unmatched");
		assertEquals("Hello Adam, is your age 29", result);
	}

	// Special tests
	@Test
	public void SameTempaltesDiffReplaceValue() {
		// Two entries in EntryMap with same templates, but different replace
		// values
		// Only the first one replace value to be used
		map.store("name", "Adam", false);
		map.store("name", "Brian", false);
		String result = engine.evaluate("${name} ${name}", map, "delete-unmatched");
		assertEquals("Adam Adam", result);
	}

	@Test
	public void MultSameMapEntries() {
		// Multiple similar entries in EntryMap
		// Only the first replace value to be used among similar templates
		map.store("name", "Brian", false);
		map.store("name", "Brian", false);
		map.store("name", "Adam", false);
		map.store("lala Brian notimportant", "Cenka Atanasova", false);
		String result = engine.evaluate("Blabla ${lala ${name} notimportant} pena ${name}", map, "delete-unmatched");
		assertEquals("Blabla Cenka Atanasova pena Brian", result);
	}

	@Test
	public void EntryMapDumplicates() {
		// Checks if the total number of entries in EntryMap is correct
		map.store("name", "Brian", false);
		map.store("name", "Brian", true);
		map.store("name", "Adam", false);
		assertEquals(3, map.getEntries().size());
	}

	@Test
	public void EntryMapDumplicates2() {
		// Checks if duplicate entries in EntryMap are removed
		map.store("name", "Brian", false);
		map.store("name", "Brian", false);
		map.store("name", "Adam", false);
		assertEquals(2, map.getEntries().size());
	}

	@Test
	public void NullTestEngineTemplate() {
		// Engine template == null
		// To return the template string as it is
		map.store("name", "Adam", false);
		String result = engine.evaluate(null, map, "delete-unmatched");
		assertEquals(null, result);
	}

	@Test
	public void NullTestEngineMap() {
		// Evaluate map == null
		// To return the template string as it is
		String result = engine.evaluate("${name}", null, "delete-unmatched");
		assertEquals("${name}", result);
	}

	@Test // (expected = Exception.class)
	public void NullTestMatchingMode() {
		// Evaluate matching_mode == null
		// Matching mode expected to default to 'delete-unmatched'
		map.store("name", "Adam", false);
		engine.evaluate("${name ${does not match}}", map, null);
	}

	@Test(expected = Exception.class)
	public void NullTestMapTemplate() {
		// Map template == null
		// Runtime exception expected
		map.store(null, "Adam", false);
	}

	@Test(expected = Exception.class)
	public void NullTestReplaceValue() {
		// map replace_value == null
		map.store("name", null, false);
	}
 
	/*@Test(expected = Exception.class)
	public void NullTestCaseFlag() {
		// Map case_sensitive_flag == null
		// Runtime exception expected
		map.store("Name", "Adam", null);
		String result = engine.evaluate("${name}", map, "delete-umatched");
		assertEquals("Adam", result);
	}*/

	@Test
	public void EmptyMatchMode() {
		// Matching mode is an empty string
		// Expected matching mode to default to 'delete-unmatched'
		map.store("name", "Adam", false);
		map.store("surname", "Dykes", false);
		String result = engine.evaluate("${won't match} ${name} ${won't match either}", map, "");
		assertEquals(" Adam ", result);
	}

	@Test
	public void WrongMatchMode() {
		// Matching mode is a string which is not 'keep-unmatched' or
		// 'delete-unmatched'
		// Expected matching mode to default to 'delete-unmatched'
		map.store("name", "Adam", false);
		map.store("surname", "Dykes", false);
		String result = engine.evaluate("${won't match} ${name} ${won't match either}", map, "wrong");
		assertEquals(" Adam ", result);
	}

	@Test
	public void WrongInputMatchingMode() {
		// Evaluate matching_mode == wrong; Expected to default to
		// 'delete-unmatched'
		// Non-visible characters test; Expected to be ignored
		// Case sensitivity; case sensitivity to be ignored with case
		// sensitivity flag false, and considered otherwise
		map.store("Name", "Adam", true);
		map.store("Name", "Brian", false);
		map.store("Name Brian", "Adam Brian", true);
		String result = engine.evaluate("${Na m		e ${does not match}    ${name}} is ${Name}", map,
				"something-wrong");
		assertEquals("Adam Brian is Adam", result);
	}

	@Test
	public void TemplateInReplaceValue() {
		// Have a template in place of a replace value
		// Do not expect the template in question to be considered as such
		map.store("first1", "${first2}", false);
		map.store("first2", "Brian", false);
		String result = engine.evaluate("${first1} ${first2} ${first1}", map, "something-wrong");
		assertNotEquals("Brian Brian Brian", result);
	}

	@Test
	public void TemplateInReplaceValue2() {
		// Have a template in place of a replace value
		// Expect the template in question to be simply printed out as it is
		map.store("first1", "${first2}", false);
		map.store("first2", "Brian", false);
		String result = engine.evaluate("${first1} ${first2} ${first1}", map, "something-wrong");
		assertEquals("${first2} Brian ${first2}", result);
	}

	// Tests based on spreadsheet table containing test categories

	@Test
	public void OrderingMap() {
		// Tests if it takes the first entry of a template when the same
		// template is entered
		// more than once
		map.store("name", "Adam", false);
		map.store("name", "Brian", false);
		map.store("Surname", "Collins", true);
		map.store("Surname", "Dykes", true);
		String result = engine.evaluate("Hello ${name} ${Surname}", map, "delete-unmatched");
		assertEquals("Hello Adam Collins", result);
	}

	@Test
	public void OrderingTemplate() {
		// Tests if it takes the shorter templates before the long ones
		map.store("nesting long ${first}", "wrong", false);
		map.store("first", "middle step", false);
		map.store("nesting long middle step", "correct", true);
		String result = engine.evaluate("${nesting long ${first}}", map, "delete-unmatched");
		assertEquals("correct", result);
	}

	@Test
	public void VisibleChars1() {
		// Tests if it ignores non-visible chars
		// when ignoring capitals and deleting non-matches
		map.store("N a M e", "Adam", false);
		map.store("Sur		name", "Dykes", false);
		map.store("adje\nctive", "hired", false);
		String result = engine.evaluate(
				"Hello ${nA${won't match}mE\n} ${surname}, you are ${AD		ject I v E}!${NoT gonna match :P}", map,
				"delete-unmatched");
		assertEquals("Hello Adam Dykes, you are hired!", result);
	}

	@Test
	public void VisibleChars2() {
		// Tests if it ignores non-visible chars
		// when it's case sensitive and keeps non-matches
		map.store("N a M e", "Adam", true);
		map.store("Sur		name", "Dykes", true);
		map.store("ADje\nctIvE", "hired", true);
		map.store("not gonna match :(", " ... ", true);
		String result = engine.evaluate(
				"Hello ${NaMe} ${Sur \n name}, you are ${AD		ject I v E}! ${NoT gonna match :(}", map,
				"keep-unmatched");
		assertEquals("Hello Adam Dykes, you are hired! ${NoT gonna match :(}", result);
	}

	@Test
	public void VisibleChars3() {
		// Tests if it ignores non-visible chars
		// when ignoring capitals and deleting non-matches
		map.store("N a M e", "Adam", false);
		map.store("Sur		name", "Dykes", false);
		map.store("adje\nctive", "hired", false);
		String result = engine.evaluate(
				"Hello ${nA${won't match}mE} ${sur \n name}, you are ${AD		ject I v E}!${NoT gonna match :P}", map,
				"delete-unmatched");
		assertEquals("Hello Adam Dykes, you are hired!", result);
	}

	@Test
	public void NonClosed() {
		map.store("end", " end", false);
		String result = engine.evaluate("${some free space${  end}", map, "keep-unmatched");
		assertEquals("${some free space end", result);
	}

	@Test
	public void NonClosed2() {
		map.store("fullStop", ".", false);
		String result = engine.evaluate("${ } ${   ${  fullStop}", map, "delete-unmatched");
		assertEquals(" ${   .", result);
	}

	@Test
	public void NonClosedTemplates() {
		// Checks if rules for start of patterns is correctly adhered to
		// with no case sensitivity and deleting non-matches
		map.store("something", "starting boundaries", false);
		String result = engine.evaluate("This has too ${many ${something ${or too few brackets}}.", map,
				"delete-unmatched");
		assertEquals("This has too ${many starting boundaries.", result);
	}

	@Test
	public void NonClosedTemplates2() {
		// Checks if rules for start of patterns is correctly adhered to
		// with case sensitivity and keeping non-matches
		map.store("someThing", "starting boundaries", true);
		String result = engine.evaluate("This has too ${many ${someThing} ${or too few closing brackets}.", map,
				"keep-unmatched");
		assertEquals("This has too ${many starting boundaries ${or too few closing brackets}.", result);
	}

	@Test
	public void NonOpenedTemplates() {
		// Checks if rules for start of patterns is correctly adhered to
		// with no case sensitivity and deleting non-matches
		map.store("something", "starting boundaries", false);
		String result = engine.evaluate("This has too few ${something ${or too many closing brackets}}}.", map,
				"delete-unmatched");
		assertEquals("This has too few starting boundaries}.", result);
	}

	@Test
	public void NonOpenedTemplates2() {
		// Checks if rules for start of patterns is correctly adhered to
		// with case sensitivity and keeping non-matches
		map.store("Something", "starting boundaries", true);
		String result = engine.evaluate("This has too few ${Something} ${or too many closing brackets}}}.", map,
				"keep-unmatched");
		assertEquals("This has too few starting boundaries ${or too many closing brackets}}}.", result);
	}

	/*@Test
	public void WrongOpenTemplate() {
		
		 * Templates opened in a wrong way. Specifications are not clear on what
		 * should happen when there is space between the dollar sign and the
		 * opening curly bracket. Here we assume it should recognise the
		 * template and remove '$', '{' and all the space between them
		 
		map.store("Name", "Adam", false);
		map.store("Name", "Brian", true);
		String result = engine.evaluate("$  {name} \n{name} \n$      {Name}", map, "delete-unmatched");
		assertEquals("Adam \n{name} \nAdam", result);
	}*/

	@Test
	public void SameNameTemplate() {
		// With no case sensitivity and deleting non-matches
		map.store("Name", "Adam", false);
		map.store("Name", "Brian", false);
		String result = engine.evaluate("${na${no-match}me} ${sound like no-mage}${name}", map, "delete-unmatched");
		assertEquals("Adam Adam", result);
	}

	@Test
	public void SameNameTemplate2() {
		// With case sensitivity and deleting non-matches
		map.store("Name", "Adam", true);
		map.store("Name", "Brian", true);
		String result = engine.evaluate("${Na${no-match}me} ${sound like no-mage}${Name}", map, "delete-unmatched");
		assertEquals("Adam Adam", result);
	}

	@Test
	public void SameNameTemplate3() {
		// With no case sensitivity and keeping non-matches
		map.store("Name", "Adam", false);
		map.store("Name", "Brian", false);
		String result = engine.evaluate("${na${no-match}me} ${sound like no-mage} ${name}", map, "keep-unmatched");
		assertEquals("${na${no-match}me} ${sound like no-mage} Adam", result);
	}

	@Test
	public void SameNameTemplate4() {
		// With case sensitivity and keeping non-matches
		map.store("Name", "Adam", true);
		map.store("Name", "Brian", true);
		String result = engine.evaluate("${Na${no-match}me} ${sound like no-mage} ${Name}", map, "keep-unmatched");
		assertEquals("${Na${no-match}me} ${sound like no-mage} Adam", result);
	}

	@Test
	public void EmptyTemplate() {
		// Evaluate template is an empty string
		map.store("name", "Adam", false);
		map.store("surname", "Dykes", false);
		String result = engine.evaluate("", map, "delete-unmatched");
		assertEquals("", result);
	}

	@Test
	public void EmptyMap() {
		/*
		 * The map is empty but it is not null (it is initialised but nothing
		 * has been stored) The specifications do not cover the case and we
		 * assume that instead of printing out the template string as if it was
		 * supplied with a null argument, the desired behaviour would be to
		 * delete the unmatched templates and print the remainder of the input
		 * when in "delete-unmatched" mode. 
		 */
		String result = engine.evaluate("This${won't match}${and this}${won't match either}", map, "delete-unmatched");
		assertEquals("This", result);
	}

	@Test
	public void SingleTemplate() {
		// With no case sensitivity and deleting the unmatched templates
		map.store("name", "Adam", false);
		String result = engine.evaluate("Hello ${name}!", map, "delete-unmatched");
		assertEquals("Hello Adam!", result);
	}

	@Test
	public void SameLengthTemplatesLeftToRight() {
		// Same length templates should be processed from left to right
		// However, this case cannot be tested based purely on the output
		// We need to step through the program line by line
		// The test below shows our best attempt at covering this case, yet
		// unsuccessful
		map.store("same1", "same2", false);
		map.store("same2", "something", false);
		map.store("something", "correct", false);
		String result = engine.evaluate("${${same1}} ${${same2}}", map, "keep-unmatched");
		assertEquals("something correct", result);
	}

	/*
	 * @Test public void SameLengthTemplatesLeftToRight2() { // Same length
	 * templates should be processed from left to right // However, this case
	 * cannot be tested based purely on the output // We need to step through
	 * the program line by line // The test below shows our best attempt at
	 * covering this case, yet unsuccessful map.store("same1", "same2", false);
	 * map.store("same2", "something", false); map.store("something", "correct",
	 * false); String result = engine.evaluate("${${same2}} ${${same1}}",
	 * map,"delete-unmatched"); assertEquals("correct ${same2}", result); }
	 */

	@Test
	public void SingleTempleCaseSenseKeepNoMatch0() {
		// With case sensitivity and keeping the unmatched templates
		// and mixed case replace value
		map.store("forename", "PeTeR", true);
		String result = engine.evaluate("Hello, my name is ${forename}", map, "keep-unmatched");
		assertEquals("Hello, my name is PeTeR", result);
	}

	@Test
	public void SingleTempleCaseSenseKeepNoMatch1() {
		// With case sensitivity and keeping the unmatched templates
		// and mixed case template
		map.store("SuRnAmE", "stefanov", true);
		String result = engine.evaluate("Hello, my name is ${SuRnAmE}", map, "keep-unmatched");
		assertEquals("Hello, my name is stefanov", result);
	}

	@Test
	public void SingleTempleCaseSenseKeepNoMatch2() {
		// With case sensitivity and keeping the unmatched templates
		// and mixed case template
		map.store("foreNAME", "peter", true);
		String result = engine.evaluate("Hello, my name is ${forename}", map, "keep-unmatched");
		assertNotEquals("Hello, my name is peter", result);
	}

	@Test
	public void SingleTempleCaseSenseKeepNoMatch3() {
		// With case sensitivity and keeping the unmatched templates
		// and mixed case replace value
		map.store("surname", "StEfAnOv", true);
		String result = engine.evaluate("Hello, my name is ${surNAME}", map, "keep-unmatched");
		assertNotEquals("Hello, my name is StEfAnOv", result);
	}

	@Test
	public void SingleTempleNoCaseSenseKeepNoMatch0() {
		// With no case sensitivity and keeping the unmatched templates
		// and mixed case replace value
		map.store("forename", "PeTeR", false);
		String result = engine.evaluate("Hello, my name is ${forename}", map, "keep-unmatched");
		assertEquals("Hello, my name is PeTeR", result);
	}

	@Test
	public void SingleTempleNoCaseSenseKeepNoMatch1() {
		// With no case sensitivity and keeping the unmatched templates
		// and mixed case template
		// and mixed case template in template string
		map.store("SuRnAmE", "stefanov", false);
		String result = engine.evaluate("Hello, my name is ${SuRnAmE}", map, "keep-unmatched");
		assertEquals("Hello, my name is stefanov", result);
	}

	@Test
	public void SingleTempleNoCaseSenseKeepNoMatch2() {
		// With no case sensitivity and keeping the unmatched templates
		// and mixed case template
		// and normal templte in template string
		map.store("foreNAME", "peter", false);
		String result = engine.evaluate("Hello, my name is ${forename}", map, "keep-unmatched");
		assertEquals("Hello, my name is peter", result);
	}

	@Test
	public void SingleTempleNoCaseSenseKeepNoMatch3() {
		// With no case sensitivity and keeping the unmatched templates
		// and mixed case replace value
		// and mixed case template in template string
		map.store("surname", "StEfAnOv", false);
		String result = engine.evaluate("Hello, my name is ${surNAME}", map, "keep-unmatched");
		assertEquals("Hello, my name is StEfAnOv", result);
	}

	@Test
	public void SingleTempleCaseSenseDeleteNoMatch0() {
		// With case sensitivity and deleting the unmatched templates
		// and mixed case replace value
		map.store("forename", "PeTeR", true);
		String result = engine.evaluate("Hello, my name is ${forename}${extra}", map, "delete-unmatched");
		assertEquals("Hello, my name is PeTeR", result);
	}

	@Test
	public void SingleTempleCaseSenseDeleteNoMatch1() {
		// With case sensitivity and deleting the unmatched templates
		// and mixed case template
		// and mixed case template in template string
		map.store("SuRnAmE", "stefanov", true);
		String result = engine.evaluate("Hello, my name${extra} is ${SuRnAmE}", map, "delete-unmatched");
		assertEquals("Hello, my name is stefanov", result);
	}

	@Test
	public void SingleTempleCaseSenseDeleteNoMatch2() {
		// With case sensitivity and deleting the unmatched templates
		// and mixed case template
		// and normal template in template string
		map.store("foreNAME", "peter", true);
		String result = engine.evaluate("Hello, my${extra} name is ${forename}", map, "delete-unmatched");
		assertNotEquals("Hello, my name is peter", result);
	}

	@Test
	public void SingleTempleCaseSenseDeleteNoMatch3() {
		// With case sensitivity and deleting the unmatched templates
		// and mixed case replace value
		// and mixed case template in template string
		map.store("surname", "StEfAnOv", true);
		String result = engine.evaluate("Hello,${extra} my name is ${surNAME}", map, "delete-unmatched");
		assertNotEquals("Hello, my name is StEfAnOv", result);
	}

	@Test
	public void SingleTempleNoCaseSenseDeleteNoMatch0() {
		// With no case sensitivity and deleting the unmatched templates
		// and mixed case replace value
		map.store("forename", "PeTeR", false);
		String result = engine.evaluate("Hello, my name is ${forename${extra}}", map, "delete-unmatched");
		assertEquals("Hello, my name is PeTeR", result);
	}

	@Test
	public void SingleTempleNoCaseSenseDeleteNoMatch1() {
		// With no case sensitivity and deleting the unmatched templates
		// and mixed case template
		// and mixed case template in template string
		map.store("SuRnAmE", "stefanov", false);
		String result = engine.evaluate("Hello, my name is ${${extra}SuRnAmE}", map, "delete-unmatched");
		assertEquals("Hello, my name is stefanov", result);
	}

	@Test
	public void SingleTempleNoCaseSenseDeleteNoMatch2() {
		// With no case sensitivity and deleting the unmatched templates
		// and mixed case template
		// and normal case template in template string
		map.store("foreNAME", "peter", false);
		map.store("useless", "-", false);
		String result = engine.evaluate("${extra}Hello, my name is ${forename}", map, "delete-unmatched");
		assertEquals("Hello, my name is peter", result);
	}

	@Test
	public void SingleTempleNoCaseSenseDeleteNoMatch3() {
		// With no case sensitivity and deleting the unmatched templates
		// and mixed case replace value
		// and mixed case template in template string
		map.store("surname", "StEfAnOv", false);
		map.store("-", "useless", false);
		String result = engine.evaluate("Hello, my name is ${surNAME}", map, "delete-unmatched");
		assertEquals("Hello, my name is StEfAnOv", result);
	}

	@Test
	public void MultTempleCaseSenseKeepNoMatch0() {
		// With case sensitivity and keeping the unmatched templates
		// and pairs with mixed case templates and replace values
		map.store("forename", "PeTeR", true);
		map.store("SuRnAmE", "stefanov", true);
		String result = engine.evaluate("Hello, my name is ${forename} ${SuRnAmE}", map, "keep-unmatched");
		assertEquals("Hello, my name is PeTeR stefanov", result);
	}

	@Test
	public void MultTempleCaseSenseKeepNoMatch1() {
		// With case sensitivity and keeping the unmatched templates
		// and pairs with mixed case templates and replace values
		map.store("foreNAME", "peter", true);
		map.store("surname", "StEfAnOv", true);
		String result = engine.evaluate("Hello, my name is ${forename} ${surNAME}", map, "keep-unmatched");
		assertNotEquals("Hello, my name is peter StEfAnOv", result);
	}

	@Test
	public void MultTempleNoCaseSenseKeepNoMatch0() {
		// With no case sensitivity and keeping the unmatched templates
		// and pairs with mixed case templates and replace values
		map.store("forename", "PeTeR", false);
		map.store("SuRnAmE", "stefanov", false);
		String result = engine.evaluate("Hello, my name is ${forename} ${SuRnAmE}", map, "keep-unmatched");
		assertEquals("Hello, my name is PeTeR stefanov", result);
	}

	@Test
	public void MultTempleNoCaseSenseKeepNoMatch1() {
		// With no case sensitivity and keeping the unmatched templates
		// and pairs with mixed case templates and replace values
		map.store("foreNAME", "peter", false);
		map.store("surname", "StEfAnOv", false);
		String result = engine.evaluate("Hello, my name is ${forename} ${surNAME}", map, "keep-unmatched");
		assertEquals("Hello, my name is peter StEfAnOv", result);
	}

	@Test
	public void MultTempleCaseSenseDeleteNoMatch0() {
		// With case sensitivity and deleting the unmatched templates
		// and pairs with mixed case templates and replace values
		map.store("forename", "PeTeR", true);
		map.store("SuRnAmE", "stefanov", true);
		String result = engine.evaluate("Hello, my name is ${forename}${extra} ${SuRnAmE}", map, "delete-unmatched");
		assertEquals("Hello, my name is PeTeR stefanov", result);
	}

	@Test
	public void MultTempleCaseSenseDeleteNoMatch1() {
		// With case sensitivity and deleting the unmatched templates
		// and pairs with mixed case templates and replace values
		map.store("foreNAME", "peter", true);
		map.store("surname", "StEfAnOv", true);
		String result = engine.evaluate("Hello, my${extra} name is ${forename} ${surNAME}", map, "delete-unmatched");
		assertNotEquals("Hello, my name is peter StEfAnOv", result);
	}

	@Test
	public void MultTempleNoCaseSenseDeleteNoMatch0() {
		// With no case sensitivity and deleting the unmatched templates
		// and pairs with mixed case templates and replace values
		map.store("forename", "PeTeR", false);
		map.store("SuRnAmE", "stefanov", false);
		String result = engine.evaluate("Hello, my name is ${forename${extra}} ${${extra}SuRnAmE}", map,
				"delete-unmatched");
		assertEquals("Hello, my name is PeTeR stefanov", result);
	}

	@Test
	public void MultTempleNoCaseSenseDeleteNoMatch1() {
		// With no case sensitivity and deleting the unmatched templates
		// and pairs with mixed case templates and replace values
		map.store("foreNAME", "peter", false);
		map.store("useless", "-", false);
		map.store("surname", "StEfAnOv", false);
		map.store("-", "useless", false);
		String result = engine.evaluate("${extra}Hello, my name is ${forename} ${surNAME}", map, "delete-unmatched");
		assertEquals("Hello, my name is peter StEfAnOv", result);
	}

	@Test
	public void NestTempleCaseSenseKeepNoMatch() {
		// Nested templates in a template string
		// with case sensitivity and keeping the unmatched templates
		// Non English characters used within replace value
		map.store("nickname", "Pete", true);
		map.store("Pete", "Peter", true);
		map.store("real", "Петър", true);
		String result = engine.evaluate("Hello, my name is ${${nickname}}, or properly ${real}", map, "keep-unmatched");
		assertEquals("Hello, my name is Peter, or properly Петър", result);
	}

	@Test
	public void NestTempleNoCaseSenseKeepNoMatch() {
		// Nested templates in a template string
		// with no case sensitivity and keeping the unmatched templates
		// Non English characters used within templates and replace values
		map.store("NICKNAME", "ПЕШ", false);
		map.store("ПЕШ", "ПЕШОO", false);
		map.store("REAL", "Pete", false);
		String result = engine.evaluate("Hello, my name is ${${nickname}}, or properly ${real}", map, "keep-unmatched");
		assertEquals("Hello, my name is ПЕШОO, or properly Pete", result);
	}

	@Test
	public void NestTempleCaseSenseDeleteNoMatch() {
		// Nested templates in a template string
		// with case sensitivity and deleting the unmatched templates
		// and special characters - within replace values
		map.store("forename", "Peter", true);
		map.store("surname", "Stefanov", true);
		map.store("PeterStefanov", "not-gonna-tell-ya", true);
		String result = engine.evaluate("Hello, my name is ${${forename}${nothing}${surname}}", map,
				"delete-unmatched");
		assertEquals("Hello, my name is not-gonna-tell-ya", result);
	}

	@Test
	public void NestTempleNoCaseSenseDeleteNoMatch0() {
		// Nested templates in a template string
		// with no case sensitivity and deleting the unmatched templates
		// and mixed case templates
		map.store("orBLAHnaMblah", "end", false);
		map.store("forename", "PeTeR", false);
		map.store("e", "blah", false);
		String result = engine.evaluate("${${ff}or${e}nam${e}}", map, "delete-unmatched");
		assertEquals("end", result);
	}

	@Test
	public void NestTempleNoCaseSenseDeleteNoMatch1() {
		// Nested templates in a template string
		// with no case sensitivity and deleting the unmatched templates
		// and empty replace value
		map.store("forename", "", false);
		String result = engine.evaluate("${Hello, my name is }${forename}", map, "delete-unmatched");
		assertEquals("", result);
	}

	@Test
	public void TemplatePairsJointlyCompriseRealTemplate() {
		// Templates from EntryMap do not match any templates on their own
		// but they do jointly
		// Expected no match
		map.store("name", "Pete", false);
		map.store("surname", "Stefanov", false);
		String result = engine.evaluate("${namesurname}", map, "keep-unmatched");
		assertNotEquals("PeteStefanov", result);
	}

	@Test
	public void TemplatePairsJointlyCompriseRealTemplate2() {
		// Templates from EntryMap do not match any templates on their own
		// but they do jointly
		// Expected no match
		map.store("name", "Pete", false);
		map.store("surname", "Stefanov", false);
		String result = engine.evaluate("${namesurname}", map, "delete-unmatched");
		assertEquals("", result);
	}

}
