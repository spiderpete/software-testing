package st;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

//import st.EntryMap.Entry;
import st.EntryMap;
import st.TemplateEngine;

public class Task22 {
 
	private EntryMap map;

	private TemplateEngine engine;

	@Before
	public void setUp() throws Exception {
		map = new EntryMap();
		engine = new TemplateEngine();
	}
	
 
	// TASK 2 PART 2 
	
	// Mutations: 0, 5, 7
	@Test
	public void SingleTempleNoCaseSenseDeleteNoMatch1() {
		// With no case sensitivity and deleting the unmatched templates
		// and mixed case template
		// and mixed case template in template string
		map.store("SuRnAmE", "stefanov", false);
		String result = engine.evaluate("Hello, my name is ${${extra}SuRnAmE}", map, "delete-unmatched");
		assertEquals("Hello, my name is stefanov", result);
	}
	
	// Mutations: 0, 5, 7 
	@Test
	public void MultTempleNoCaseSenseDeleteNoMatch0() {
		// With no case sensitivity and deleting the unmatched templates
		// and pairs with mixed case templates and replace values
		map.store("forename", "PeTeR", false);
		map.store("SuRnAmE", "stefanov", false);
		String result = engine.evaluate("Hello, my name is ${forename${extra}} ${${extra}SuRnAmE}", map,
				"delete-unmatched");
		assertEquals("Hello, my name is PeTeR stefanov", result);
	}
	
	// Mutations: 0, 3, 5, 7
	@Test
	public void NestTempleNoCaseSenseDeleteNoMatch0() {
		// Nested templates in a template string
		// with no case sensitivity and deleting the unmatched templates
		// and mixed case templates
		map.store("orBLAHnaMblah", "end", false);
		map.store("forename", "PeTeR", false);
		map.store("e", "blah", false);
		String result = engine.evaluate("${${ff}or${e}nam${e}}", map, "delete-unmatched");
		assertEquals("end", result);
	}
	
	// Mutations: 0, 3, 7, 8
	@Test
	public void SameLengthTemplatesLeftToRight() {
		// Same length templates should be processed from left to right
		// However, this case cannot be tested based purely on the output
		// We need to step through the program line by line
		// The test below shows our best attempt at covering this case, yet
		// unsuccessful
		map.store("same1", "same2", false);
		map.store("same2", "something", false);
		map.store("something", "correct", false);
		String result = engine.evaluate("${${same1}} ${${same2}}", map, "keep-unmatched");
		assertEquals("something correct", result);
	}
	
	// Mutations: 0, 3
	@Test
	public void TemplateIfEmptyTemplateString() {
		// Tests how we deal with an empty template

		// Attempts to test the branch in the doReplace() method which covers
		// the template being at the end of the input/instanced string.
		// The test doesn't cover the branch because the indices are counted
		// from 0,
		// while the length starts from 1
		map.store("name", "Adam", false);
		map.store("surname", "Dykes", false);
		String result = engine.evaluate("${}", map, "delete-unmatched");
		assertEquals("", result);
	}
	
	// Mutations: 0, 2, 3
	@Test
	public void TwoConsecutiveEmptyTemplatesDelete() {
		// it checks that two consecutive empty templates are properly deleted when not matched
		map.store("name", "Adam", false);
		map.store("surname", "Dykes", false);
		String result = engine.evaluate("${}${}", map, "delete-unmatched");
		assertEquals("", result);
	}
	
	// Mutations: 0, 3, 7, 8
	@Test
	public void NestedDelete() {
		// checks that the code can deal with multiple nested templates when deleting the unmatched
		map.store("name", "surname", false);
		map.store("surname", "name", false);
		String result = engine.evaluate("${${${name}}} ${something} ${${${name}}}", map, "delete-unmatched");
		assertEquals("surname  surname", result);
	}
	
	// Mutations: 0, 6, 7
	@Test
	public void NestTempleCaseSenseKeepNoMatch() {
		// Nested templates in a template string
		// with case sensitivity and keeping the unmatched templates
		// Non English characters used within replace value
		map.store("nickname", "Pete", true);
		map.store("Pete", "Peter", true);
		map.store("real", "Петър", true);
		String result = engine.evaluate("Hello, my name is ${${nickname}}, or properly ${real}", map, "keep-unmatched");
		assertEquals("Hello, my name is Peter, or properly Петър", result);
	}
	
	// Mutations: 0, 3, 7, 8, 9 
	@Test
	public void NestedKeep() {
		// checks that the code can deal with multiple nested templates when keeping the unmatched
		map.store("name", "surname", false);
		map.store("surname", "name", false);
		String result = engine.evaluate("${${${name}}} ${something} ${${${name}}}", map, "keep-unmatched");
		assertEquals("surname ${something} surname", result);
	}
	
	// Mutations: 0, 6, 7
	@Test
	public void NestTempleCaseSenseDeleteNoMatch() {
		// Nested templates in a template string
		// with case sensitivity and deleting the unmatched templates
		// and special characters - within replace values
		map.store("forename", "Peter", true);
		map.store("surname", "Stefanov", true);
		map.store("PeterStefanov", "not-gonna-tell-ya", true);
		String result = engine.evaluate("Hello, my name is ${${forename}${nothing}${surname}}", map,
				"delete-unmatched");
		assertEquals("Hello, my name is not-gonna-tell-ya", result);
	}
	
	// Mutations: 0, 5, 7
	@Test
	public void NestTempleNoCaseSenseKeepNoMatch() {
		// Nested templates in a template string
		// with no case sensitivity and keeping the unmatched templates
		// Non English characters used within templates and replace values
		map.store("NICKNAME", "ПЕШ", false);
		map.store("ПЕШ", "ПЕШОO", false);
		map.store("REAL", "Pete", false);
		String result = engine.evaluate("Hello, my name is ${${nickname}}, or properly ${real}", map, "keep-unmatched");
		assertEquals("Hello, my name is ПЕШОO, or properly Pete", result);
	}
	
	// Mutations: 1
	@Test
	public void WrongOpenTemplate() {
		//templates open in the wrong way
		map.store("Name", "Adam", false);
		map.store("Name", "Brian", true);
		String result = engine.evaluate("$  {name} \n{name} \n$      {Name}", map, "delete-unmatched");
		assertEquals("$  {name} \n{name} \n$      {Name}", result);
	}
	
	// Mutations: 3, 5
	@Test
	public void SingleTempleNoCaseSenseDeleteNoMatch2() {
		// With no case sensitivity and deleting the unmatched templates
		// and mixed case template
		// and normal case template in template string
		map.store("foreNAME", "peter", false);
		map.store("useless", "-", false);
		String result = engine.evaluate("${extra}Hello, my name is ${forename}", map, "delete-unmatched");
		assertEquals("Hello, my name is peter", result);
	}
	
	// Mutations: 3, 5
	@Test
	public void MultTempleNoCaseSenseDeleteNoMatch1() {
		// With no case sensitivity and deleting the unmatched templates
		// and pairs with mixed case templates and replace values
		map.store("foreNAME", "peter", false);
		map.store("useless", "-", false);
		map.store("surname", "StEfAnOv", false);
		map.store("-", "useless", false);
		String result = engine.evaluate("${extra}Hello, my name is ${forename} ${surNAME}", map, "delete-unmatched");
		assertEquals("Hello, my name is peter StEfAnOv", result);
	}
	
	// Mutations: 3
	@Test
	public void NestTempleNoCaseSenseDeleteNoMatch1() {
		// Nested templates in a template string
		// with no case sensitivity and deleting the unmatched templates
		// and empty replace value
		map.store("forename", "", false);
		String result = engine.evaluate("${Hello, my name is }${forename}", map, "delete-unmatched");
		assertEquals("", result);
	}
	
	// Mutations: 3, 7
	@Test
	public void OrderingTemplate() {
		// Tests if it takes the shorter templates before the long ones
		map.store("nesting long ${first}", "wrong", false);
		map.store("first", "middle step", false);
		map.store("nesting long middle step", "correct", true);
		String result = engine.evaluate("${nesting long ${first}}", map, "delete-unmatched");
		assertEquals("correct", result);
	}
	
	// Mutations: 3, 5
	@Test
	public void NonClosed2() {
		map.store("fullStop", ".", false);
		String result = engine.evaluate("${ } ${   ${  fullStop}", map, "delete-unmatched");
		assertEquals(" ${   .", result);
	}
	
	// Mutations: 3
	@Test
	public void TemplateInReplaceValue2() {
		// Have a template in place of a replace value
		// Expect the template in question to be simply printed out as it is
		map.store("first1", "${first2}", false);
		map.store("first2", "Brian", false);
		String result = engine.evaluate("${first1} ${first2} ${first1}", map, "something-wrong");
		assertEquals("${first2} Brian ${first2}", result);
	}
	
	// Mutations: 3, 5, 6, 7
	@Test
	public void WrongInputMatchingMode() {
		// Evaluate matching_mode == wrong; Expected to default to
		// 'delete-unmatched'
		// Non-visible characters test; Expected to be ignored
		// Case sensitivity; case sensitivity to be ignored with case
		// sensitivity flag false, and considered otherwise
		map.store("Name", "Adam", true);
		map.store("Name", "Brian", false);
		map.store("Name Brian", "Adam Brian", true);
		String result = engine.evaluate("${Na m		e ${does not match}    ${name}} is ${Name}", map,
				"something-wrong");
		assertEquals("Adam Brian is Adam", result);
	}
	
	// Mutations: 3
	@Test
	public void WrongMatchMode() {
		// Matching mode is a string which is not 'keep-unmatched' or
		// 'delete-unmatched'
		// Expected matching mode to default to 'delete-unmatched'
		map.store("name", "Adam", false);
		map.store("surname", "Dykes", false);
		String result = engine.evaluate("${won't match} ${name} ${won't match either}", map, "wrong");
		assertEquals(" Adam ", result);
	}
	
	// Mutations: 3
	@Test
	public void TemplatePairsJointlyCompriseRealTemplate2() {
		// Templates from EntryMap do not match any templates on their own
		// but they do jointly
		// Expected no match
		map.store("name", "Pete", false);
		map.store("surname", "Stefanov", false);
		String result = engine.evaluate("${namesurname}", map, "delete-unmatched");
		assertEquals("", result);
	}
	
	// Mutations: 3, 6, 7, 8
	@Test
	public void SameNameTemplate2() {
		// With case sensitivity and deleting non-matches
		map.store("Name", "Adam", true);
		map.store("Name", "Brian", true);
		String result = engine.evaluate("${Na${no-match}me} ${sound like no-mage}${Name}", map, "delete-unmatched");
		assertEquals("Adam Adam", result);
	}
	
	// Mutations: 3
	@Test
	public void TemplateIfNull() {
		// was meant to test what happens if the sort function does not enter
		// the for loop
		// however, we were unable to come up with a case where this would
		// happen
		map.store("name", "Adam", false);
		map.store("surname", "Dykes", false);
		String result = engine.evaluate("${name}", map, "delete-unmatched");
		assertEquals("Adam", result);
	}
	
	// Mutations: 3
	@Test
	public void EmptyMatchMode() {
		// Matching mode is an empty string
		// Expected matching mode to default to 'delete-unmatched'
		map.store("name", "Adam", false);
		map.store("surname", "Dykes", false);
		String result = engine.evaluate("${won't match} ${name} ${won't match either}", map, "");
		assertEquals(" Adam ", result);
	}
	
	// Mutations: 3, 5, 7, 8
	@Test
	public void SameNameTemplate() {
		// With no case sensitivity and deleting non-matches
		map.store("Name", "Adam", false);
		map.store("Name", "Brian", false);
		String result = engine.evaluate("${na${no-match}me} ${sound like no-mage}${name}", map, "delete-unmatched");
		assertEquals("Adam Adam", result);
	}
	
	// Mutations: 3
	@Test
	public void SameTempaltesDiffReplaceValue() {
		// Two entries in EntryMap with same templates, but different replace
		// values
		// Only the first one replace value to be used
		map.store("name", "Adam", false);
		map.store("name", "Brian", false);
		String result = engine.evaluate("${name} ${name}", map, "delete-unmatched");
		assertEquals("Adam Adam", result);
	}
	
	// Mutations: 3, 5
	@Test
	public void NullTestCaseFlag2() {
		// Map case_sensitive_flag == null
		// Runtime exception expected
		map.store("Name", "Adam", null);
		String result = engine.evaluate("${name}", map, "delete-umatched");
		assertEquals("Adam", result);
	}
	
	// Mutations: 4
	@Test
	public void EntryMapDumplicates2() {
		// Checks if duplicate entries in EntryMap are removed
		map.store("name", "Brian", false);
		map.store("name", "Brian", false);
		map.store("name", "Adam", false);
		assertEquals(2, map.getEntries().size());
	}
	
	// Mutations: 4
	@Test
	public void EntryMapDumplicates() {
		// Checks if the total number of entries in EntryMap is correct
		map.store("name", "Brian", false);
		map.store("name", "Brian", true);
		map.store("name", "Adam", false);
		assertEquals(3, map.getEntries().size());
	}
	
	// Mutations: 5, 7
	@Test
	public void MultSameMapEntries() {
		// Multiple similar entries in EntryMap
		// Only the first replace value to be used among similar templates
		map.store("name", "Brian", false);
		map.store("name", "Brian", false);
		map.store("name", "Adam", false);
		map.store("lala Brian notimportant", "Cenka Atanasova", false);
		String result = engine.evaluate("Blabla ${lala ${name} notimportant} pena ${name}", map, "delete-unmatched");
		assertEquals("Blabla Cenka Atanasova pena Brian", result);
	}
	
	// Mutations: 5
	@Test
	public void MultTempleNoCaseSenseKeepNoMatch0() {
		// With no case sensitivity and keeping the unmatched templates
		// and pairs with mixed case templates and replace values
		map.store("forename", "PeTeR", false);
		map.store("SuRnAmE", "stefanov", false);
		String result = engine.evaluate("Hello, my name is ${forename} ${SuRnAmE}", map, "keep-unmatched");
		assertEquals("Hello, my name is PeTeR stefanov", result);
	}
	
	// Mutations: 5
	@Test
	public void MultTempleNoCaseSenseKeepNoMatch1() {
		// With no case sensitivity and keeping the unmatched templates
		// and pairs with mixed case templates and replace values
		map.store("foreNAME", "peter", false);
		map.store("surname", "StEfAnOv", false);
		String result = engine.evaluate("Hello, my name is ${forename} ${surNAME}", map, "keep-unmatched");
		assertEquals("Hello, my name is peter StEfAnOv", result);
	}
	
	// Mutations: 5
	@Test
	public void SingleTempleNoCaseSenseKeepNoMatch1() {
		// With no case sensitivity and keeping the unmatched templates
		// and mixed case template
		// and mixed case template in template string
		map.store("SuRnAmE", "stefanov", false);
		String result = engine.evaluate("Hello, my name is ${SuRnAmE}", map, "keep-unmatched");
		assertEquals("Hello, my name is stefanov", result);
	}
	
	// Mutations: 5
	@Test
	public void SingleTempleNoCaseSenseKeepNoMatch2() {
		// With no case sensitivity and keeping the unmatched templates
		// and mixed case template
		// and normal templte in template string
		map.store("foreNAME", "peter", false);
		String result = engine.evaluate("Hello, my name is ${forename}", map, "keep-unmatched");
		assertEquals("Hello, my name is peter", result);
	}
	
	// Mutations: 5
	@Test
	public void SameNameTemplate3() {
		// With no case sensitivity and keeping non-matches
		map.store("Name", "Adam", false);
		map.store("Name", "Brian", false);
		String result = engine.evaluate("${na${no-match}me} ${sound like no-mage} ${name}", map, "keep-unmatched");
		assertEquals("${na${no-match}me} ${sound like no-mage} Adam", result);
	}
	
	// Mutations: 5
	@Test
	public void VisibleChars1() {
		// Tests if it ignores non-visible chars
		// when ignoring capitals and deleting non-matches
		map.store("N a M e", "Adam", false);
		map.store("Sur		name", "Dykes", false);
		map.store("adje\nctive", "hired", false);
		String result = engine.evaluate(
				"Hello ${nA${won't match}mE\n} ${surname}, you are ${AD		ject I v E}!${NoT gonna match :P}", map,
				"delete-unmatched");
		assertEquals("Hello Adam Dykes, you are hired!", result);
	}
	
	// Mutations: 5, 7, 8
	@Test
	public void VisibleChars3() {
		// Tests if it ignores non-visible chars
		// when ignoring capitals and deleting non-matches
		map.store("N a M e", "Adam", false);
		map.store("Sur		name", "Dykes", false);
		map.store("adje\nctive", "hired", false);
		String result = engine.evaluate(
				"Hello ${nA${won't match}mE} ${sur \n name}, you are ${AD		ject I v E}!${NoT gonna match :P}", map,
				"delete-unmatched");
		assertEquals("Hello Adam Dykes, you are hired!", result);
	}
	
	// Mutations: 6
	@Test
	public void NonClosedTemplates2() {
		// Checks if rules for start of patterns is correctly adhered to
		// with case sensitivity and keeping non-matches
		map.store("someThing", "starting boundaries", true);
		String result = engine.evaluate("This has too ${many ${someThing} ${or too few closing brackets}.", map,
				"keep-unmatched");
		assertEquals("This has too ${many starting boundaries ${or too few closing brackets}.", result);
	}
	
	// Mutations: 6
	@Test
	public void MultTempleCaseSenseDeleteNoMatch0() {
		// With case sensitivity and deleting the unmatched templates
		// and pairs with mixed case templates and replace values
		map.store("forename", "PeTeR", true);
		map.store("SuRnAmE", "stefanov", true);
		String result = engine.evaluate("Hello, my name is ${forename}${extra} ${SuRnAmE}", map, "delete-unmatched");
		assertEquals("Hello, my name is PeTeR stefanov", result);
	}
	
	// Mutations: 6
	@Test
	public void SingleTempleCaseSenseDeleteNoMatch1() {
		// With case sensitivity and deleting the unmatched templates
		// and mixed case template
		// and mixed case template in template string
		map.store("SuRnAmE", "stefanov", true);
		String result = engine.evaluate("Hello, my name${extra} is ${SuRnAmE}", map, "delete-unmatched");
		assertEquals("Hello, my name is stefanov", result);
	}
	
	// Mutations: 6
	@Test
	public void SingleTempleCaseSenseDeleteNoMatch3() {
		// With case sensitivity and deleting the unmatched templates
		// and mixed case replace value
		// and mixed case template in template string
		map.store("surname", "StEfAnOv", true);
		String result = engine.evaluate("Hello,${extra} my name is ${surNAME}", map, "delete-unmatched");
		assertNotEquals("Hello, my name is StEfAnOv", result);
	}
	
	// Mutations: 6
	@Test
	public void OrderingMap() {
		// Tests if it takes the first entry of a template when the same
		// template is entered
		// more than once
		map.store("name", "Adam", false);
		map.store("name", "Brian", false);
		map.store("Surname", "Collins", true);
		map.store("Surname", "Dykes", true);
		String result = engine.evaluate("Hello ${name} ${Surname}", map, "delete-unmatched");
		assertEquals("Hello Adam Collins", result);
	}
	
	// Mutations: 6
	@Test
	public void NonOpenedTemplates2() {
		// Checks if rules for start of patterns is correctly adhered to
		// with case sensitivity and keeping non-matches
		map.store("Something", "starting boundaries", true);
		String result = engine.evaluate("This has too few ${Something} ${or too many closing brackets}}}.", map,
				"keep-unmatched");
		assertEquals("This has too few starting boundaries ${or too many closing brackets}}}.", result);
	}
	
	// Mutations: 6
	@Test
	public void SameNameTemplate4() {
		// With case sensitivity and keeping non-matches
		map.store("Name", "Adam", true);
		map.store("Name", "Brian", true);
		String result = engine.evaluate("${Na${no-match}me} ${sound like no-mage} ${Name}", map, "keep-unmatched");
		assertEquals("${Na${no-match}me} ${sound like no-mage} Adam", result);
	}
	
	// Mutations: 6
	@Test
	public void MultTempleCaseSenseKeepNoMatch0() {
		// With case sensitivity and keeping the unmatched templates
		// and pairs with mixed case templates and replace values
		map.store("forename", "PeTeR", true);
		map.store("SuRnAmE", "stefanov", true);
		String result = engine.evaluate("Hello, my name is ${forename} ${SuRnAmE}", map, "keep-unmatched");
		assertEquals("Hello, my name is PeTeR stefanov", result);
	}
	
	// Mutations: 6
	@Test
	public void VisibleChars2() {
		// Tests if it ignores non-visible chars
		// when it's case sensitive and keeps non-matches
		map.store("N a M e", "Adam", true);
		map.store("Sur		name", "Dykes", true);
		map.store("ADje\nctIvE", "hired", true);
		map.store("not gonna match :(", " ... ", true);
		String result = engine.evaluate(
				"Hello ${NaMe} ${Sur \n name}, you are ${AD		ject I v E}! ${NoT gonna match :(}", map,
				"keep-unmatched");
		assertEquals("Hello Adam Dykes, you are hired! ${NoT gonna match :(}", result);
	}
	
	// Mutations: 6
	@Test
	public void SingleTempleCaseSenseKeepNoMatch1() {
		// With case sensitivity and keeping the unmatched templates
		// and mixed case template
		map.store("SuRnAmE", "stefanov", true);
		String result = engine.evaluate("Hello, my name is ${SuRnAmE}", map, "keep-unmatched");
		assertEquals("Hello, my name is stefanov", result);
	}
	
	// Mutations: 6 
	@Test
	public void SingleTempleCaseSenseKeepNoMatch3() {
		// With case sensitivity and keeping the unmatched templates
		// and mixed case replace value
		map.store("surname", "StEfAnOv", true);
		String result = engine.evaluate("Hello, my name is ${surNAME}", map, "keep-unmatched");
		assertNotEquals("Hello, my name is StEfAnOv", result);
	}
	
	// Mutations: 7
	@Test
	public void SingleTempleNoCaseSenseDeleteNoMatch0() {
		// With no case sensitivity and deleting the unmatched templates
		// and mixed case replace value
		map.store("forename", "PeTeR", false);
		String result = engine.evaluate("Hello, my name is ${forename${extra}}", map, "delete-unmatched");
		assertEquals("Hello, my name is PeTeR", result);
	}
}
