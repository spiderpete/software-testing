package st;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

//import st.EntryMap.Entry;
import st.EntryMap;
import st.TemplateEngine;

public class Task31 {
 
	private EntryMap map;

	private TemplateEngine engine;

	@Before
	public void setUp() throws Exception {
		map = new EntryMap();
		engine = new TemplateEngine();
	}
	
 
	// TASK 3 PART 1 
	
	@Test
	public void ReplaceSameNumber() {
		//  keep-unmatched and delete-unmatched replace the same number of simple templates
		map.store("name", "Adam", false);
		String result = engine.evaluate("Hello ${name} ${surname}", map, "optimization");
		assertEquals("Hello Adam ${surname}", result);
	}
	
	@Test
	public void ReplaceSameNumber2() {
		// keep-unmatched and delete-unmatched replace the same number of simple templates
		map.store("name", "Adam", false);
		map.store("surname", "Dykes", false);
		String result = engine.evaluate("Hello ${name} ${middlename} ${surname} ${second surname}", map, "optimization");
		assertEquals("Hello Adam ${middlename} Dykes ${second surname}", result);
	}
	
	/* The test passes the current implementation because the matching mode defaults
	 * to delete-unmatched unless specified as "keep-unmatched"
	@Test
	public void DeleteReplacesMore() {
		// optimization defaults to delete-unmatched
		map.store("name", "Adam", false);
		map.store("surname", "Dykes", false);
		map.store("age", "29", false);
		String result = engine.evaluate("Hello ${name}, is your age ${age ${symbol}}", map, "optimization");
		assertEquals("Hello Adam, is your age 29", result);
	}*/
	
	@Test
	public void KeepReplacesMore() {
		// keep-unmatched replaces more templates than delete-unmatched with shallow nesting
		map.store("name", "Adam", false);
		map.store("surname", "Dykes", false);
		map.store("age ${symbol}", "29", false);
		String result = engine.evaluate("Hello ${name}, is your age ${age ${symbol}}", map, "optimization");
		assertEquals("Hello Adam, is your age 29", result);
	}
	
	@Test
	public void ReplaceSameNumberNested() {
		// keep-unmatched and delete-unmatched replace the same number of shallowly nested templates
		map.store("name", "Adam", false);
		map.store("surname", "Dykes", false);
		map.store("age ${symbol}", "29", false);
		map.store("question", "correct?", false);
		String result = engine.evaluate("Hello ${name}, your age is ${age ${symbol}}, ${question${mark}}", map, "optimization");
		assertEquals("Hello Adam, your age is 29, ${question${mark}}", result);
	}
	
	@Test
	public void KeepReplacesMoreNested() {
		// keep-unmatched replaces more templates than delete-unmatched with multiple shallow nesting
		// keep- and delete- unmatched replace the same number of templates, so the tie is to be broken by using keep-unmatched
		map.store("a", "I", false);
		map.store("abc", "Sam", false);
		//map.store("age ${symbol ${something ${more nesting ${to cover loops}}}}", "29", false);
		String result = engine.evaluate("${${a} ${ab} ${abc}!}", map, "optimization");
		assertEquals("${I ${ab} Sam!}", result);
	}
	
	@Test
	public void KeepReplacesMoreDeleteCurrentlyUsedNested() {
		// Nested templates, currently defaulting to delete-unmatched, but has to be keep-unmatched
		// keep-unmatched replaces more templates currently defaulting to delete-unmatched
		map.store("a", "I", false);
		map.store("abc", "Sam", false);
		map.store("I ${ab} Sam!", "I am Sam!", false);
		String result = engine.evaluate("${${a} ${ab} ${abc}!}", map, "optimization");
		assertEquals("I am Sam!", result);
	}
	
	@Test
	public void KeepReplacesMoreDeleteCurrentlyUsedNested2() {
		// Deep nesting of templates, currently using delete-unmatched, has to use keep-unmatched
		map.store("a${b${v${g${d}}}}", "I", false);
		map.store("abc", "Sam", false);
		map.store("I ${ab} Sam!", "I am Sam!", false);
		String result = engine.evaluate("${${a${b${v${g${d}}}}} ${ab} ${abc}!}", map, "optimization");
		assertEquals("I am Sam!", result);
	}
	
}
