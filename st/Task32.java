package st;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

//import st.EntryMap.Entry;
import st.EntryMap;
import st.TemplateEngine;

public class Task32 {
 
	private EntryMap map;

	private TemplateEngine engine;

	@Before
	public void setUp() throws Exception {
		map = new EntryMap();
		engine = new TemplateEngine();
	}
	
	
	// TASK 3 PART 2
	
	// This test was commented out in our previous submission because it passes on the original implementation
	// where "optimization" mode is not implemented. This happens because the matching mode defaults to "delete-unmatched"
	// when it sees a random string, which would include "optimization" at that stage. Since the aim of the test is to
	// choose "delete-unmatched", the final result is the same.
	@Test
	public void DeleteReplacesMore() {
		// optimization chooses to delete-unmatched
		
		// We uncommented this test once we reached the point at which our code was passing all the tests submitted 
		// for the first part. However, one thing we could not test with the allowed tests for part 3.1 was 
		// optimisation choosing delete-unmatched, so we are testing it now for completion.
		// Upon adding the test, it did not pass the implementation and we had to expand the implementation.
		map.store("name", "Adam", false);
		map.store("surname", "Dykes", false);
		map.store("age", "29", false);
		String result = engine.evaluate("Hello ${name}, is your age ${age ${symbol}}", map, "optimization");
		assertEquals("Hello Adam, is your age 29", result);
	}
	
	// We added this test together with the above one for better coverage, which together with the test above sparked the need for
	// the last piece of code being added, specifically lines of code 29 to 35 of TemplateEngine.java
	@Test
	public void DeleteReplacesMoreNested() {
		// delete-unmatched replaces more templates than keep-unmatched with multiple shallow nesting
		map.store("a", "I", false);
		map.store("abc", "Sam", false);
		map.store("I  Sam!", "Sam!", false);
		String result = engine.evaluate("${${a${}} ${ab${}} ${abc${}}!}", map, "optimization");
		assertEquals("Sam!", result);
	}
	
	// TASK 3 PART 1 
	
	@Test
	public void ReplaceSameNumber() {
		//  keep-unmatched and delete-unmatched replace the same number of simple templates
		map.store("name", "Adam", false);
		String result = engine.evaluate("Hello ${name} ${surname}", map, "optimization");
		assertEquals("Hello Adam ${surname}", result);
	}
	
	@Test
	public void ReplaceSameNumber2() {
		// keep-unmatched and delete-unmatched replace the same number of simple templates
		map.store("name", "Adam", false);
		map.store("surname", "Dykes", false);
		String result = engine.evaluate("Hello ${name} ${middlename} ${surname} ${second surname}", map, "optimization");
		assertEquals("Hello Adam ${middlename} Dykes ${second surname}", result);
	}
	
	@Test
	public void KeepReplacesMore() {
		// keep-unmatched replaces more templates than delete-unmatched with shallow nesting
		map.store("name", "Adam", false);
		map.store("surname", "Dykes", false);
		map.store("age ${symbol}", "29", false);
		String result = engine.evaluate("Hello ${name}, is your age ${age ${symbol}}", map, "optimization");
		assertEquals("Hello Adam, is your age 29", result);
	}
	
	@Test
	public void ReplaceSameNumberNested() {
		// keep-unmatched and delete-unmatched replace the same number of shallowly nested templates
		map.store("name", "Adam", false);
		map.store("surname", "Dykes", false);
		map.store("age ${symbol}", "29", false);
		map.store("question", "correct?", false);
		String result = engine.evaluate("Hello ${name}, your age is ${age ${symbol}}, ${question${mark}}", map, "optimization");
		assertEquals("Hello Adam, your age is 29, ${question${mark}}", result);
	}
	
	@Test
	public void KeepReplacesMoreNested() {
		// keep-unmatched replaces more templates than delete-unmatched with multiple shallow nesting
		// keep- and delete- unmatched replace the same number of templates, so the tie is to be broken by using keep-unmatched
		map.store("a", "I", false);
		map.store("abc", "Sam", false);
		//map.store("age ${symbol ${something ${more nesting ${to cover loops}}}}", "29", false);
		String result = engine.evaluate("${${a} ${ab} ${abc}!}", map, "optimization");
		assertEquals("${I ${ab} Sam!}", result);
	}
	
	@Test
	public void KeepReplacesMoreDeleteCurrentlyUsedNested() {
		// Nested templates, currently defaulting to delete-unmatched, but has to be keep-unmatched
		// keep-unmatched replaces more templates currently defaulting to delete-unmatched
		map.store("a", "I", false);
		map.store("abc", "Sam", false);
		map.store("I ${ab} Sam!", "I am Sam!", false);
		String result = engine.evaluate("${${a} ${ab} ${abc}!}", map, "optimization");
		assertEquals("I am Sam!", result);
	}
	
	@Test
	public void KeepReplacesMoreDeleteCurrentlyUsedNested2() {
		// Deep nesting of templates, currently using delete-unmatched, has to use keep-unmatched
		map.store("a${b${v${g${d}}}}", "I", false);
		map.store("abc", "Sam", false);
		map.store("I ${ab} Sam!", "I am Sam!", false);
		String result = engine.evaluate("${${a${b${v${g${d}}}}} ${ab} ${abc}!}", map, "optimization");
		assertEquals("I am Sam!", result);
	}
	
}
