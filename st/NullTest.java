package st;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
 
//import st.EntryMap.Entry;
import st.EntryMap;
import st.TemplateEngine;

public class NullTest {

	private EntryMap map;

	private TemplateEngine engine;

	@Before
	public void setUp() throws Exception {
		map = new EntryMap();
		engine = new TemplateEngine();
	}
	 
	// FAILS EVERYTHING 

	// Mutation (erros, as opposed to failures like on original source): 3
	@Test(expected = Exception.class)
	public void NullTestCaseFlag() {
		// Map case_sensitive_flag == null
		// Runtime exception expected
		map.store("Name", "Adam", null);
		String result = engine.evaluate("${name}", map, "delete-umatched");
		assertEquals("Adam", result);
	}
	
	@Test
	public void NullTestCaseFlag2() {
		// Map case_sensitive_flag == null
		// Runtime exception expected
		map.store("Name", "Adam", null);
		String result = engine.evaluate("${name}", map, "delete-umatched");
		assertEquals("Adam", result);
	}


}
