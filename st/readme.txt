ABOUT THE TEST FILE:

The file Task21.java contains all of our tests from the first coursework plus the tests we have added for the second one. The tests for task 2 are at the top of the file to make it easier to read, beginning under the tittle comment //TASK 2 PART 1

They include our tests on the equals() methods under the subheading comment // Testing equals() bellow: 
In order for them to work the file Task21.java needs to be in the "st" package. If you run it outside the package, please comment out everything between the comments "// Testing equals() bellow: " and "//TASK 1"


ABOUT THE SCREENSHOTS:

coverage-1.jpg shows our coverage before starting task 2
coverage-2.jpg shows our coverage upon completing task 2, including the tests on the equals() methods
coverage-without-equals.jpg shows our coverage upon completing task 2 but without the tests on the equals() methods